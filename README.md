# nuxtjs-gentelella

Gentelella Admin is a free to use Bootstrap admin template.
This Project is a revamp of the original admin template created by
[@colorlib](https://twitter.com/colorlib).

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
